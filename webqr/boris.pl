#!/usr/bin/env perl
use Mojolicious::Lite;
use Imager::QRCode;

get '/' => sub {
    my $c = shift;

    my $got_request = $c->param('message') ? 1 : 0;

    $c->session->{message} = $c->param('message') if $got_request;

    $c->render(template => 'index', got_request => $got_request);
};

get '/qr' => sub {
    my $c = shift;

    my $qrcode = '';
    Imager::QRCode->new()
                  ->plot($c->session->{message})
                  ->write(data => \$qrcode, type => 'png')
                  if (    exists $c->session->{message}
                      and length $c->session->{message} > 0);
    $c->render(data => $qrcode);
};

app->start;
__DATA__

@@ index.html.ep
% layout 'default';
% title 'Welcome';
<h1>QR-Code</h1>
<form action="/">
Message: <input type="text" name="message"><br>
<br>
<input type="submit" value="send">
</form>
<%== $got_request ? '<img src="/qr" />' : '<!-- qr code -->' %>

@@ layouts/default.html.ep
<!DOCTYPE html>
<html>
  <head><title><%= title %></title></head>
  <body><%= content %></body>
</html>
