#!/usr/bin/env perl
use Mojolicious::Lite;
use Image::PNG::QRCode 'qrpng';

my $TRANSPARENT_IMAGE =
    pack('H86', '47494638396101000100800100000000FFFFFF21F904010A0001002C00000000010001000002024C01003B');

get '/' => sub {
    my ($connection) = @_;

    $connection->session->{message} = $connection->param('message');

    $connection->render(template => 'index');
};

get '/qr' => sub {
    my ($connection) = @_;

    my $message = $connection->session->{message};
    my $qrcode = $TRANSPARENT_IMAGE;
    if ($message) {
        $qrcode = qrpng(text => $message);
    }

    $connection->render(data => $qrcode);
};

app->start;
__DATA__

@@ index.html.ep
% layout 'default';
% title 'Welcome';
<h1>QR-Code</h1>
<form action="/">
Message: <input type="text" name="message"><br>
<br>
<input type="submit" value="send">
</form>
<img src="/qr" />

@@ layouts/default.html.ep
<!DOCTYPE html>
<html>
  <head><title><%= title %></title></head>
  <body><%= content %></body>
</html>
