# first: install perl + cpanm via perlbrew

cpanm Image::PNG::QRCode
cpanm Mojolicious

# if you start from scratch
mojo generate lite_app qr.pl

# if you start from this repo
git clone https://gitlab.com/borisdaeppen/Perl-Training.git
cd webqr

# run the app for testing
perl qr.pl daemon
